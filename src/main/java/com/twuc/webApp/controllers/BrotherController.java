package com.twuc.webApp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BrotherController {
    @GetMapping("/api/brother-errors/illegal-argument")
    public void brotherController() {
        throw new IllegalArgumentException("brother");
    }
}
