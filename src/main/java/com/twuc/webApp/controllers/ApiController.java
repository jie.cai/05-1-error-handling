package com.twuc.webApp.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
@RequestMapping("/api")
public class ApiController {

    // # 2.1

    @GetMapping("/exceptions/runtime-exception")
    public void getRuntimeException() {
        throw new RuntimeException("wow");
    }

    @ExceptionHandler
    public ResponseEntity<String> handleRuntimeException(RuntimeException exception) {
       return ResponseEntity.status(400).body(400 + " " + exception.getMessage());
    }

    // # 2.2

    @GetMapping("/exceptions/access-control-exception")
    public void getAccessControlException() {
        throw new AccessControlException("deny");
    }

    @ExceptionHandler
    public ResponseEntity<String> handleAccessControlException(AccessControlException exception) {
        return ResponseEntity.status(403).body(403 + " " + exception.getMessage());
    }

    // # 2.3

    @GetMapping("/errors/null-pointer")
    public void getNullPointerException() {
        throw new NullPointerException("null");
    }

    @GetMapping("/errors/arithmetic")
    public void getArithmeticException() {
        throw new ArithmeticException("arithmetic");
    }

    @ExceptionHandler({ NullPointerException.class, ArithmeticException.class })
    public ResponseEntity<String> handleDoubleError(Exception exception) {
        return ResponseEntity.status(418).body(418 + " " + exception.getMessage());
    }
}
