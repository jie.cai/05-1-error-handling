package com.twuc.webApp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SisterController {
    @GetMapping("/api/sister-errors/illegal-argument")
    public void sisterController() {
        throw new IllegalArgumentException("sister");
    }
}
