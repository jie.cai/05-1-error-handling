package com.twuc.webApp.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class AdviceControllerTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_handle_illegal_argument_exception_from_brother_controller() {
        ResponseEntity<String> result = testRestTemplate.getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertEquals(418, result.getStatusCodeValue());
        assertEquals("handle exception by advice handler from brother", result.getBody());
    }

    @Test
    void should_handle_illegal_argument_exception_from_sister_controller() {
        ResponseEntity<String> result = testRestTemplate.getForEntity("/api/sister-errors/illegal-argument", String.class);
        assertEquals(418, result.getStatusCodeValue());
        assertEquals("handle exception by advice handler from sister", result.getBody());
    }
}
