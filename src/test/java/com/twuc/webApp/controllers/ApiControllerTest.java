package com.twuc.webApp.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class ApiControllerTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_get_exception_of_runtime_exception() {
        int statusCodeValue = testRestTemplate
                .getForEntity("/api/exceptions/runtime-exception", String.class)
                .getStatusCodeValue();
        assertEquals(statusCodeValue, 400);
    }

    @Test
    void should_get_exception_of_access_control_exception() {
        int statusCodeValue = testRestTemplate
                .getForEntity("/api/exceptions/access-control-exception", String.class)
                .getStatusCodeValue();
        assertEquals(statusCodeValue, 403);
    }

    @Test
    void should_get_null_pointer_exception_with_418() {
        int statusCodeValue = testRestTemplate
                .getForEntity("/api/errors/null-pointer", String.class)
                .getStatusCodeValue();
        assertEquals(statusCodeValue, 418);
    }

    @Test
    void should_get_arithmetic_exception_with_418() {
        int statusCodeValue = testRestTemplate
                .getForEntity("/api/errors/arithmetic", String.class)
                .getStatusCodeValue();
        assertEquals(statusCodeValue, 418);
    }
}
